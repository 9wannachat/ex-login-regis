import { Component } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';
import { RegisterPage } from '../register/register';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public form: FormGroup;
  constructor(public navCtrl: NavController, private formBuilder: FormBuilder, private toastCtrl: ToastController) {
    this.form = this.formBuilder.group({
      username: ['',Validators.required],
      password : ['', Validators.required]
    });
  }

  register(){
    this.navCtrl.push(RegisterPage);
  }

  loginForm(){
    console.log(this.form.value);
    let toast = this.toastCtrl.create({
      message: 'Login successfully',
      duration: 3000,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }


}
